import boto3
import pickle
import logging
import pandas as pd
import numpy as np
import pyarrow.parquet as pq
import xgboost as xgb
from typing import Tuple, List, Union, Callable
from datetime import date
from xgboost import XGBClassifier
from snowflake.connector.connection import SnowflakeConnection
from xgboost import XGBClassifier
from sklearn.model_selection import train_test_split
from sklearn.metrics import roc_auc_score, average_precision_score
from hyperopt import STATUS_OK, Trials, fmin, hp, tpe
from functools import partial
logger = logging.getLogger(__name__)
import s3fs
s3 = s3fs.S3FileSystem()

### Generic helper functions ###

def write_pickle_to_s3(bucket, key, data):
    """Function to write pickle files to s3"""
    
    s3 = boto3.client('s3')
    serialized = pickle.dumps(data)
    s3.put_object(Bucket=bucket, Key=key, Body=serialized)
    
    
def read_pickle_from_s3(bucket, key):
    """Function to read pickle files from s3"""
    
    s3 = boto3.client('s3')
    response = s3.get_object(Bucket=bucket, Key=key)
    body = response['Body'].read()
    return pickle.loads(body)

### Functions to read and combine and split data ###

def read_labels(label_path: str) -> pd.DataFrame:
    """Function to read in label data and return a dataframe.  This is only
    executed if the diagnostics flag is set to True.
    
    Parameters
    ----------
    label_path: str
        The full path to the features created by the SDF job
       
    
    Returns
    -------
    A pandas DataFrame contining discount preference labels by INDV_ID
    """
    
    logger.info('Beginning reading of label table')
    label_table = pd.read_csv(label_path, header=0)
    label_table.drop_duplicates('INDV_ID', inplace=True)
    logger.info(f'Read label table with {len(label_table)} observations')
    
    return label_table
    

def read_features(feature_path: str) -> Tuple[pd.DataFrame, date]:
    """Function to read in feature data and feature generation date
    
    Parameters
    ----------
    feature_path: str
        The full path to the features created by the SDF job
    
    Returns
    -------
    A tuple containing the pandas DataFrame with the features and the score date
    """
    
    # Read feature table
    feature_table = pq.ParquetDataset(feature_path, filesystem=s3).read_pandas().to_pandas()
    feature_table.drop_duplicates('INDV_ID', inplace=True)
    
    # Create new features
    feature_table['ecomm_frac'] = feature_table['numEcom']/feature_table['numPurchase']
    feature_table['is_sp'] = feature_table['LOYALTY_NUMBER'].apply(lambda x: 1 if x > 0 else 0)
    feature_table['is_ecomm'] = feature_table['ecomm_frac'].apply(lambda x: 1 if x > 0.0 else 0)
    score_date = feature_table.loc[0,'CLV_DATE']
    logger.info(f'Read feature table with {len(feature_table)} observations')
    
    return feature_table, score_date


def read_models(bucket: str, 
                ecomm_key: str, 
                no_ecomm_key: str) -> Tuple[XGBClassifier, XGBClassifier, List[str], List[str], date]:
    """Function to read in the models and return XGB model objects and feature lists
    
    Parameters
    ----------
    bucket: str
        A string containing the bucket that holds the fitted models
        
    ecomm_key: str
        A string containing the key for the eCommerce engaged model
        
    no_ecomm_key: str
        A string containing the key for the non-eCommerce engaged model    
    
    Returns
    -------
    A tuple containing the XGBoost classifers for both of the models, 
    and the feature lists for both of the models
    """
    
    # Read models and extract feature names
    model_yes = read_pickle_from_s3(BUCKET, ECOMM_KEY)
    yes_features = model_yes.get_booster().feature_names
    logger.info(f'Read eCommerce engaged model with {len(yes_features)} features')
    model_no = read_pickle_from_s3(BUCKET, NO_ECOMM_KEY)
    no_features = model_no.get_booster().feature_names
    logger.info(f'Read non-eCommerce engaged model with with {len(no_features)} features')    
    return model_yes, model_no, yes_features, no_features


def read_extra_features(aap_query: str, 
                        comp_query: str, 
                        tenure_query: str, 
                        ecomm_xref_query: str,
                        effify: Callable,
                        conn: SnowflakeConnection) -> Tuple[pd.DataFrame, pd.DataFrame, pd.DataFrame]:
    """Function to read in tables to create extra features and return these as pandas DataFrames
    
    Parameters
    ----------
    aap_query: str
        The query string to find the distance to the closest AAP store by INDV_ID
    
    comp_query: str
        The query string to find the distance to the closest compeitor store by INDV_ID
        
    tenure_query: str
        The query string to find the number of days since the customers first purchase by INDV_ID
        
    ecomm_xref_query: str
        The query string to create a cross reference table between INDV_ID and ECOMM_MEMBER_ID
        
    effify: Callable
        A function to evaluate f-strings
        
    conn: SnowflakeConnection
    
    Returns
    -------
    A tuple containing four pandas DataFrames, one for each of the derived features
    """
    
    aap_dist = pd.read_sql(effify(aap_query), conn)
    aap_dist.drop_duplicates('INDV_ID', inplace=True)
    aap_dist = aap_dist[['INDV_ID', 'DIST']].rename(columns={'DIST': 'AAP_DIST'})
    logger.info(
        f'Read AAP distance table with {len(aap_dist)} rows')
    comp_dist = pd.read_sql(effify(comp_query), conn)
    comp_dist.drop_duplicates('INDV_ID', inplace=True)
    comp_dist = comp_dist[['INDV_ID', 'DIST']].rename(columns={'DIST': 'CMP_DIST'})
    logger.info(
        f'Read competitor distance table with {len(comp_dist)} rows')                
    tenure = pd.read_sql(effify(tenure_query), conn)
    tenure.drop_duplicates('INDV_ID', inplace=True)
    logger.info(
        f'Read tenure table with {len(tenure)} rows')                 
    ecomm_xref = pd.read_sql(effify(ecomm_xref_query), conn)
    ecomm_xref.drop_duplicates(inplace=True)
    ecomm_xref.dropna(inplace=True)
    logger.info(
        f'Read eCommrece XREF table with {ecomm_xref.INDV_ID.nunique()} INDV_IDs and {ecomm_xref.ECOMM_MEMBER_ID.nunique()} ecomm member ids')
    
    conn.close()    
    return aap_dist, comp_dist, tenure, ecomm_xref


def combine_features(feature_table: pd.DataFrame, 
                     aap_dist: pd.DataFrame, 
                     comp_dist: pd.DataFrame, 
                     tenure: pd.DataFrame, 
                     label_table: Union[pd.DataFrame, None],
                     diag: bool) -> pd.DataFrame:
    """Function to combined feature table with extra feature tables
    
    Parameters
    ----------
    feature_table: pd.DataFrame
        The pandas DataFrame with the features from spark by INDV_ID
    
    aap_dist: pd.DataFrame
        The pandas DataFrame with the distances to the closest AAP store by INDV_ID
        
    comp_dist: pd.DataFrame
        The pandas DataFrame with the distances to the closest competitor store by INDV_ID
        
    tenure: pd.DataFrame
        The pandas DataFrame with the number of days since the customers first purchase by INDV_ID
        
    label_table: Union[pd.DataFrame, None]
        Either a pandas Dataframe with the labels by INDV_ID or None
        
    diag: bool
        True if labels to be uese either for diagnostics or training
    
    Returns
    -------
    A pandas DataFrames with both original and addtional features
    """
    
    # AAP store distances
    feature_table = pd.merge(feature_table, aap_dist, how='left', on='INDV_ID')
    feature_table['AAP_DIST'] = feature_table['AAP_DIST'].fillna(feature_table['AAP_DIST'].mean())
    logger.info('Joined AAP distance to feature table')

    #Competitor store distances
    feature_table = pd.merge(feature_table, comp_dist, how='left', on='INDV_ID')
    feature_table['CMP_DIST'] = feature_table['CMP_DIST'].fillna(feature_table['CMP_DIST'].mean())
    logger.info('Joined competitor distance to feature table')

    # New features
    feature_table['aap_closer_by'] = feature_table['CMP_DIST'] - feature_table['AAP_DIST']
    feature_table['comp_dist_ratio'] = feature_table['CMP_DIST']/feature_table['AAP_DIST']
    logger.info('Added relative distances to feature table')

    # Customer lifetime (tenure)
    feature_table = pd.merge(feature_table, tenure, how='left', on='INDV_ID')
    feature_table['TENURE'] = feature_table['TENURE'].fillna(feature_table['TENURE'].mean())
    feature_table.loc[(feature_table.TENURE < 1), 'TENURE'] = 1
    logger.info('Added customer tenure to feature table')
    
    # Now combine with labels if DIAG is True
    if diag:
        combined_table = pd.merge(feature_table, label_table, on='INDV_ID', how='left')
        combined_table.drop_duplicates('INDV_ID', inplace=True)
        combined_table.dropna(inplace=True)
        logger.info(f'Joined labels to features to create combined table since diagnositcs will be produced')
    else:
        combined_table = feature_table.dropna()
        logger.info(f'Created combined table without labels since diagnositcs will not be produced')
    
    return combined_table

def split_data(combined_table: pd.DataFrame) -> Tuple[pd.DataFrame, pd.DataFrame, float, float]:
    """Function to split data into eComm engaged and non-eComm engaged"""
    # eComm engaged customers
    yes_table = combined_table[combined_table.is_ecomm==1]
    counter_yes = yes_table.groupby('3quarter').count()['INDV_ID']
    frac_positive_class_yes = counter_yes[1.000]/counter_yes.sum()
    logger.info('Created table for eComm engaged customers')
    
    # Non-eComm engaged customers
    no_table = combined_table[combined_table.is_ecomm==0]
    counter_no = no_table.groupby('3quarter').count()['INDV_ID']
    frac_positive_class_no = counter_no[1.000]/counter_no.sum()
    logger.info('Created table for non-eComm engaged customers')
    
    return yes_table, no_table, frac_positive_class_yes, frac_positive_class_no

### Functions to tune and evaluate models ###

def objective_full(space: dict,
                  X_use: pd.DataFrame,
                  y_use: pd.DataFrame,
                  X_validate: pd.DataFrame,
                  y_validate: pd.DataFrame,
                  n_estimators: int,
                  n_jobs: int,
                  early_stopping_rounds: int) -> dict:
    """Function used by Hyperopt to tune XGBoost model. This function will be instaniated into a new function
    with a single argument 'space' using 'partial'
    
    Parameters
    ----------
    space: dict
        Dictionary contining the search space for hyperopt
        
    X_use: pd.DataFrame
        Pandas DataFrame containg training features
        
    y_use: pd.DataFrame
        Pandas DataFrame containing the trainning data ground truth labels
        
    X_validate: pd.DataFrame
        Pandas DataFrame containg validation features
        
    y_validate: pd.DataFrame
        Pandas DataFrame containing the validation data ground truth labels
        
    n_estimators: int
        Number of trees to build in the XGBoost model
        
    n_jobs: int
        Number of parallel threads to use when fitting XGBoost model
        
    early_stopping_rounds: int
        Number of new trees without improvement to stop fitting
        
    Returns
    -------
    A dictionary with the keys loss and status
    """
    # Define the classifier
    xg_class = xgb.XGBClassifier(n_estimators=n_estimators, 
                              max_depth=space['max_depth'], gamma=space['gamma'],
                              reg_alpha=space['reg_alpha'], reg_lambda=space['reg_lambda'],
                              min_child_weight=space['min_child_weight'],
                              colsample_bytree=space['colsample_bytree'],
                              subsample=space['subsample'],
                              learning_rate=space['learning_rate'],
                              seed=6,
                              n_jobs=n_jobs)
    
    # Define the evaluation set
    eval_set = [(X_use, y_use), (X_validate, y_validate)]
    
    # Fit the classifier
    xg_class.fit(X_use, y_use, eval_set=eval_set, eval_metric='map',
            early_stopping_rounds=early_stopping_rounds, verbose=False)
    
    # Evaluate the classifier metrics
    results = xg_class.evals_result()
    
    # Calculate the loss
    validation_map_lost = 1.0 - results['validation_1']['map'][-1]
    logger.info(f'The loss is {validation_map_lost}')
   
    return {'loss': validation_map_lost, 'status': STATUS_OK}

def tune_model(table: pd.DataFrame,
                  features: List[str],
                  diag_label: str,
                  max_evals: int,
                  n_estimators: int,
                  n_jobs: int,
                  early_stopping_rounds: int,
                  space: dict,
                  test_size: float) -> Tuple[dict, pd.DataFrame, pd.DataFrame, pd.Series, pd.Series]:
    """Function used to tune a XGBoost model using Hyperopt
    
    Parameters
    ----------
    table: pd.DataFrame
        A dataframe containg columns for the features and a column for the label
    
    diag_label: str
        The name of the label column
        
    max_evals: int
        The maximum number of optimization epochs to run in Hyperopt
        
    n_estimators: int
        Number of trees to build in the XGBoost model
        
    n_jobs: int
        Number of parallel threads to use when fitting XGBoost model
        
    early_stopping_rounds: int
        Number of new trees without improvement to stop fitting
        
    space: dict
        The Hyperopt search space
        
    test_size: float
        The fraction of the data to be included in the test and validation sets
        
    Returns
    -------
    A dictionary containing the hyperparameters for the best model, and pandas DataFrames containing
    the split of training / test features and labels
    """
    
    # Split the data and re-split for hyperparameter tuning
    logger.info('Beginning split into training and test data')
    X_train, X_test, y_train, y_test = train_test_split(table[features], table[diag_label], 
                                                    test_size=test_size, random_state=1, stratify=table[diag_label])
    X_use, X_validate, y_use, y_validate = train_test_split(X_train, y_train, 
                                                    test_size=test_size, random_state=1, stratify=y_train)
    
    # Create a single argurment (space) objective function using the data
    logger.info('Beginning tuning of hyperparameters')
    objective = partial(objective_full, 
                        X_use=X_use, 
                        X_validate=X_validate, 
                        y_use=y_use, 
                        y_validate=y_validate, 
                        n_estimators=n_estimators,
                        n_jobs=n_jobs,
                        early_stopping_rounds=early_stopping_rounds)
    
    # Tune and extract the best hyperparameters
    best_hyperparams = fmin(fn = objective,
                            space = space,
                            algo = tpe.suggest,
                            max_evals = max_evals,
                            trials = Trials(),
                            return_argmin=False,
                            verbose=0,
                            rstate= np.random.RandomState(2))
    logger.info('Completed tuning of hyperparaeters')
    
    return best_hyperparams, X_train, X_test, y_train, y_test


def evaluate_model_fit(hyper_parameters: dict, 
                       X_train: pd.DataFrame, 
                       X_test: pd.DataFrame, 
                       y_train: pd.Series, 
                       y_test: pd.Series,
                       n_jobs: int,
                       n_estimators: int) -> Tuple[xgb.XGBClassifier, float, float]:
    """Function to evaluste an XGBoost classifier
    
     Parameters
    ----------
    hyper_parameters: dict
        A dictionary containing the hyperparameters for the XGBoost classifier
    
    X_train: pd.DataFrame
        A pandas DataFrame containing the training data
        
    X_test: pd.Dataframe
        A pandas DataFrame containing the test data
        
    y_train: pd.Series
        A pandas Series containing the labels for the training data
        
    y_test: pd.Seriex
        A pandas Series containing the labels for the test data
        
    n_jobs: int
        The maximum number of optimization epochs to run in Hyperopt
        
    n_estimators: int
        Number of trees to build in the XGBoost model
        
    Returns
    -------
    A fitted XBBoost classifer, along with the area under the pr cuve and the 
    area under the roc curve.
    """
    # Create the classifier
    logger.info('Beginning model fitting for evaluation')
    xg_class = xgb.XGBClassifier(**hyper_parameters, n_jobs=n_jobs, n_estimators=n_estimators, eval_metric='map')
    
    # We fit the model    
    _ = xg_class.fit(X_train, y_train, verbose=False)
    
    # Prepare the data for evaluation and compute metrics
    logger.info('Beginning calculation of metrics')
    test_score = pd.DataFrame(xg_class.predict_proba(X_test)).drop(columns={0}).rename(columns={1: 'SCORE'})
    area_under_pr = average_precision_score(y_test, test_score['SCORE'], pos_label=1)    
    area_under_roc = roc_auc_score(y_test, test_score['SCORE'])
    logger.info(f'Completed calculation of metrics. AUPR: {area_under_pr} AUROC: {area_under_roc}')
    
    # Combine train and test data for final fitting
    X_all = X_train.append(X_test)
    y_all = y_train.append(y_test)
    logger.info('Data appended')
    
    # Create a new unfitted classifier
    logger.info('Beginning model fitting for all data')
    xg_class = xgb.XGBClassifier(**hyper_parameters, n_jobs=n_jobs, n_estimators=n_estimators, eval_metric='map')
    
    # Fit the model on all data
    xg_class = xg_class.fit(X_all, y_all, verbose=True)
    logger.info('Model successfully fitted on all the data')

    return xg_class, area_under_pr, area_under_roc