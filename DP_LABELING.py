import numpy as np
import pandas as pd
import sys
import logging
import s3fs
import boto3
import ast
import yaml
import warnings
from datetime import date, timedelta, datetime
from sf import sf_connection
from alerting import alert
from snowflake.connector.connection import SnowflakeConnection
from query_strings import LABEL_QUERY
warnings.filterwarnings('ignore')

def effify(non_f_str: str):
    """Function to instantiate values into an f-string"""
    return eval(f'f"""{non_f_str}"""')

sys.path.append('/home/ec2-user/corp.advanceai.cdf.config/artifacts/bdf-shared-code/') # for debug

# Logging to move down to error only
loggers_to_shut_up = [
    "snowflake.connector",
]
for logger in loggers_to_shut_up:
    logging.getLogger(logger).setLevel(logging.ERROR)

# Setup logging
FORMAT = '%(asctime)s - [%(name)s] - %(levelname)s %(message)s'
logging.basicConfig(format=FORMAT, level=logging.INFO)
logger = logging.getLogger(__name__)
logger.info("Reading the yaml file")

# Argument dictionary from json file for debug only
custom_vars = '{"EMAIL_ID": "s.smith@advance-auto.com", "YAML_FILE_NAME": "config.yaml", "TEST": "True", "DATE_OVERRIDE": "2021-02-28" }'

# Read in the json file
#custom_vars = sys.argv[1]
custom_vars = ast.literal_eval(custom_vars)
EMAIL_ID = custom_vars['EMAIL_ID']
YAML_FILE_NAME = custom_vars['YAML_FILE_NAME']
DATE_OVERRIDE = custom_vars['DATE_OVERRIDE']
TEST = eval(custom_vars['TEST'])
logger.info("Finished reading in config from json file")

# decide historic run or current run
if DATE_OVERRIDE != "None":
    END_DATE = datetime.strptime(DATE_OVERRIDE, '%Y-%m-%d').date()
else:
    END_DATE = date.today()
    
# Read in the yaml file
with open(YAML_FILE_NAME) as f:
    config = yaml.load(f, Loader=yaml.SafeLoader)
        
# Extract the input locations
FITS = config['DATA_SOURCE']['FITS']
FITS_EXTN = config['DATA_SOURCE']['FITS_EXTN']
REWARD_HIST = config['DATA_SOURCE']['REWARD_HIST']
CAMPAIGN = config['DATA_SOURCE']['CAMPAIGN']

# Extract the output locations
if TEST:
    LABEL_PATH = config['DATA_DESTINATION']['LABEL_PATH_TEST']
else:
    LABEL_PATH = config['DATA_DESTINATION']['LABEL_PATH']

# Calculate the start date as one year behind the end data
START_DATE = END_DATE - timedelta(days=365)
logger.info(f'Labels to be created using data from {START_DATE} thru {END_DATE}')

def create_labels(label_query: str, 
                   conn: SnowflakeConnection):
    """Function to create a table of discount sensitivity labels and write these to S3
    
    Parameters
    ----------
    label_query: str
        The snowflake query with symbolic f-string notation
    
    conn: SnowflakeConnection
        A read only connection to snowflake
    
    Returns
    -------
    None
    """

    # Insert the correct variables into the f-string to create the query    
    query = effify(label_query)
    
    # Execute the query
    logger.info(f'Beginning query to pull data on discount preference labels')
    labels = pd.read_sql(query, conn)
    logger.info(f'Completed query extracting {len(labels)} observations to be labeled and beginning labeling')

    # Compute and add the labels
    labels['DISCOUNT_FRACTION'] = labels['DISCOUNT_COUNT']/labels['TX_COUNT']
    labels.loc[labels['DISCOUNT_FRACTION'] > 0, 'zero'] = 1
    labels.loc[labels['DISCOUNT_FRACTION'] > 0.25, 'quarter'] = 1
    labels.loc[labels['DISCOUNT_FRACTION'] > 0.5, 'half'] = 1
    labels.loc[labels['DISCOUNT_FRACTION'] > 0.75, '3quarter'] = 1
    labels.fillna(0, inplace=True)
    logger.info(f'Completed labeling and beginning subsetting and type conversion')
    
    # Reduce dataframe, change types, and write to S3
    features_and_labels = labels[['INDV_ID', 'TX_COUNT', 'DISCOUNT_FRACTION', 'zero', 'quarter', 'half', '3quarter']].copy()
    cols = list(features_and_labels.columns)
    for column in cols:
        if column not in ['INDV_ID', 'DISCOUNT_FRACTION']:
            features_and_labels.loc[:,column] = features_and_labels[column].astype('int').copy()
    logger.info('Completed type converions and beginning writing table to S3')
    features_and_labels.to_csv(LABEL_PATH, header=True, index=False)
    logger.info('Completed writing to S3')

if __name__ == '__main__':
    try:
        create_labels(LABEL_QUERY, sf_connection())
        alert(recipients = EMAIL_ID,
              message_body = f'Success creating labels using data from {START_DATE} thru {END_DATE}',
              subject = f'Label creation succeeded')
    except Exception as e:
        alert(recipients = EMAIL_ID,
              subject = f'Failed to create label table',
              message_body = f'Failed to  create labels using data from {START_DATE} thru {END_DATE} due to error {e}')
        logger.exception(e, exc_info=True)
        raise