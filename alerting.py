# import boto3
import json
import subprocess

region_name = 'us-east-1'
FunctionName = "Email-Alerting-Lambda"
AttachmentFunctionName = "Email-Alerting-Attachment-Lambda"
InvocationType = 'RequestResponse'

def alert(recipients, subject, message_body, region_name=region_name):
    '''
    This function takes 4 parameters
    recipients
    subject
    message_body
    region_name. It is now set to 'us-east-1' as default

    Usage: alert(recipients=['abhinandan.surapaneni@advance-auto.com'], subject='Alert Subject', message_body='Alert Message Body')
    Returns '0' for successful call, non-zero for un-successful call
    '''
    # lambda_client = boto3.client('lambda', region_name=region_name)
    # ## Parameters to the alerting service
    payload = {
        "recipients": recipients,
        "subject": subject,
        "message_body": message_body,
    }
    payload = json.dumps(payload)
    # ## Calling the alerting service
    # response = lambda_client.invoke(
    #     FunctionName=FunctionName,
    #     InvocationType=InvocationType,
    #     Payload=json.dumps(payload, indent=1)
    # )
    # return response
    '''The reason for using aws cli instead of boto3 is
        1. Not wanting to mess with boto3 versions
            if users of the framework need a specific version of boto3
    '''
    response = subprocess.check_call(["aws", "lambda", "invoke", "--function-name", FunctionName, "--payload",
                                      payload, "--invocation-type", InvocationType, 'response.json', "--region", region_name], shell=False)
    return response


def alerting_attachment_content(recipients, subject, message_body, attachment_body, attachment_name):
    '''
    This function takes 5 parameters
    recipients
    subject
    message_body
    attachment_body
    attachment_name

    Usage: alerting_attachment_content(recipients=['abhinandan.surapaneni@advance-auto.com'], subject='Alert Subject', message_body='Alert Message Body', attachment_body='Alert Attachment Body', attachment_name='Alert Attachment Name')
    Returns '0' for successful call, non-zero for un-successful call
    '''
    if isinstance(recipients, str):
        recipients = recipients.split()

    payload = {
        "recipients": recipients,
        "subject": subject,
        "message_body": message_body,
        "attachment_body": attachment_body,
        "attachment_name": attachment_name
    }
    payload = json.dumps(payload)
    response = subprocess.check_call(["aws", "lambda", "invoke", "--function-name", AttachmentFunctionName, "--payload",
                                      payload, "--invocation-type", InvocationType, 'response.json', "--region", region_name], shell=False)
    return response


def alerting_attachment_s3(recipients, subject, message_body, bucket_name, s3_key):
    '''
    This function takes 5 parameters
    recipients
    subject
    message_body
    bucket_name
    s3_key

    Usage: alerting_attachment_s3(recipients=['abhinandan.surapaneni@advance-auto.com'], subject='Alert Subject', message_body='Alert Message Body', bucket_name='Alert S3 Bucket Name', s3_key='Alert S3 Key')
    Returns '0' for successful call, non-zero for un-successful call
    '''
    if isinstance(recipients, str):
        recipients = recipients.split()

    payload = {
        "recipients": recipients,
        "subject": subject,
        "message_body": message_body,
        "bucket_name": bucket_name,
        "s3_key": s3_key
    }
    payload = json.dumps(payload)
    response = subprocess.check_call(["aws", "lambda", "invoke", "--function-name", AttachmentFunctionName, "--payload",
                                      payload, "--invocation-type", InvocationType, 'response.json', "--region", region_name], shell=False)
    return response
