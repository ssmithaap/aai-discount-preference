from hyperopt import hp
import numpy as np

"""Queries and other complex configuration items that are not easy to implement in a yaml file"""

AAP_QUERY = """
    select 
        LOAD_TS, 
        INDV_ID, 
        STORE_NUMBER, 
        DIST, 
        OPENED
    from(
        select 
        LOAD_TS, 
        INDV_ID, 
        STORE_NUMBER,
        DIST,
        OPENED, 
        ROW_NUMBER() over (partition by INDV_ID order by DIST) as ROWNUM
    from {AAP_STORE_DIST}
    where (INDV_ID, STORE_NUMBER, LOAD_TS) in (select INDV_ID, STORE_NUMBER, max(LOAD_TS) 
                                                from {AAP_STORE_DIST}
                                                group by INDV_ID, STORE_NUMBER) and OPENED = 1
        ) WHERE ROWNUM = 1 
    """

COMP_QUERY = """
    select 
        LOAD_TS, 
        INDV_ID, 
        CMP_STORE_NUMBER, 
        DIST, 
        OPENED
    from(
        select 
        LOAD_TS, 
        INDV_ID, 
        CMP_STORE_NUMBER,
        DIST,
        OPENED, 
        ROW_NUMBER() over (partition by INDV_ID order by DIST) as ROWNUM
    from {COMP_STORE_DIST}
    where (INDV_ID, CMP_STORE_NUMBER, LOAD_TS) in (select INDV_ID, CMP_STORE_NUMBER, max(LOAD_TS) 
                                                from {COMP_STORE_DIST}
                                                group by INDV_ID, CMP_STORE_NUMBER) and OPENED = 1
        ) WHERE ROWNUM = 1 
    """

TENURE_QUERY = """
    select 
        INDV_ID, 
        datediff(day, INDV_FIRST_TRANSACTION_DATE, '2021-03-01') as TENURE
    from {TENURE} 
    where (INDV_ID,LOAD_TS) in
        (select INDV_ID, max(LOAD_TS) from {TENURE} 
        group by INDV_ID)
        """

ECOMM_XREF_QUERY = """
    select 
        INDV_ID, 
        ECOMM_MEMBER_ID
    from {ECOMM_XREF} 
        """

LABEL_QUERY = """    
     select
        INDV_ID,
        max(LOYALTY_NUMBER) as LOYALTY_NUMBER,
        count(distinct TRANSACTION_IDENTIFIER) as TX_COUNT,
        sum(USED_SP_DISCOUNT) as SP_COUNT,
        sum(USED_AF_DISCOUNT) as AF_COUNT,
        sum(USED_ANY_DISCOUNT) as DISCOUNT_COUNT,
        sum(COUPON_DISCOUNT_PRORATION_AMT*USED_SP_DISCOUNT) as COUPON_DISCOUNT_PRORATION_AMT, 
        sum(POS_SALES_DISCOUNT_AMOUNT*USED_AF_DISCOUNT) as POS_SALES_DISCOUNT_AMOUNT        
    from    
    (select
        TRANSACTION_IDENTIFIER,
        max(INDV_ID) as INDV_ID,
        max(MAX_LOYALTY_NUMBER) as LOYALTY_NUMBER,
        max(COUPON_CEV_ID) as COUPON_CEV_ID,
        sum(COUPON_DISCOUNT_PRORATION_AMT) as COUPON_DISCOUNT_PRORATION_AMT,
        sum(POS_SALES_DISCOUNT_AMOUNT) as POS_SALES_DISCOUNT_AMOUNT,
        max(case when COUPON_DISCOUNT_PRORATION_AMT > 0 and COUPON_TYPE is null then 1 else 0 end) as USED_SP_DISCOUNT,
        max(case when POS_SALES_DISCOUNT_AMOUNT > 0 then 1 else 0 end) as USED_AF_DISCOUNT,
        case when (USED_SP_DISCOUNT+USED_AF_DISCOUNT)>0 then 1 else 0 end as USED_ANY_DISCOUNT        
    from    
    (select distinct
        INDV_ID,
        TRANSACTION_IDENTIFIER,
        COUPON_CEV_ID,
        LOYALTY_NUMBER,
        COUPON_DISCOUNT_PRORATION_AMT,
        POS_SALES_DISCOUNT_AMOUNT,
        max(LOYALTY_NUMBER) over (partition by INDV_ID) as MAX_LOYALTY_NUMBER,
        COUPON_TYPE
    from {FITS} as fits    
    inner join
    (select distinct
        TRANSACTION_ID,
        SEQ_NUMBER, INDV_ID,
        TRAN_DATE 
    from {FITS_EXTN}
    where TRAN_DATE >= '{START_DATE}' and TRAN_DATE <= '{END_DATE}'
    ) as fits_extn
    on fits.TRANSACTION_IDENTIFIER=fits_extn.TRANSACTION_ID and fits.SEQ_NUMBER=fits_extn.SEQ_NUMBER    
    left join
    (select distinct reward_type, case when reward_type in ('Save5Off5','Save10Off10','Save15Off15','Save20Off20') then 'SP 2.0'
          when reward_type in ('Save10Off40', 'Save20Off50', 'Save40Off100', 'Save5Off20') then 'SP 1.0'
          end as coupon_type,
          b.*
        from {REWARD_HIST} as a
        inner join {CAMPAIGN} as b
        on a.coupon_code = b.campaign_event_code
        where full_date >= '2017-12-01' and (reward_type in ('Save10Off40', 'Save20Off50', 'Save40Off100', 'Save5Off20')
                                           or reward_type in ('Save10Off10', 'Save15Off15', 'Save20Off20', 'Save5Off5'))
        ) as rewards
        on fits.coupon_cev_id = rewards.campaign_event_id
    where fits.TRAN_DATE >= '{START_DATE}' and fits.TRAN_DATE <= '{END_DATE}' and SALES_CHANNEL_IDENTIFIER in (1,3)
    and TRANSACTION_CLASS_FLAG = 'S' and TRAN_COUNT_IND = 'Y' and INDV_ID is not null)    
    group by TRANSACTION_IDENTIFIER order by TRANSACTION_IDENTIFIER)    
    group by INDV_ID order by INDV_ID
     """

# Search space for XGBoost 
SPACE={'max_depth': hp.choice("max_depth", np.arange(2, 11, dtype=int)),
           'gamma': hp.uniform('gamma',0.0, 5.0),
           'reg_alpha': hp.uniform('reg_alpha', 0, 1000.0),
           'reg_lambda': hp.uniform('reg_lambda',0, 1000),
           'min_child_weight': hp.uniform('min_child_weight', 0, 120),
           'colsample_bytree': hp.uniform('colsample_bytree', 0.5, 0.95),
           'learning_rate': hp.uniform('learning_rate', 0.1, 0.5),
           'subsample': hp.uniform('subsample', 0.5, 0.95)
          }