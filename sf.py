import os
import sys
from datetime import datetime
import json
from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives import serialization
from cryptography.hazmat.primitives.asymmetric import rsa, dsa
import snowflake.connector
import boto3

sm_client = boto3.client('secretsmanager', region_name='us-east-1')
response = sm_client.get_secret_value(SecretId='sfDSConnection')
data = json.loads(response['SecretString'])
sfUser = data['sfUser']
sfRSAKey = data['private_key']
sfPassword = None
if sfPassword:
    sfPassword = sfPassword.encode()


def sf_connection(sfUser=sfUser, sfRSAKey=sfRSAKey, sfPassword=sfPassword, account='aap.us-east-1', role='AAP_DATA_SCIENCE_ROLE', database='PEDW', warehouse='DATA_SCIENCE_WH', schema='PUBLIC'):
    '''
    This function returns a snowflake connection object.
    It authenticates with SF using Private key
    Connection Details:
        account='aap.us-east-1'
        role='AAP_DATA_SCIENCE_ROLE'
        database='PEDW'
        warehouse='DATA_SCIENCE_WH'
        schema='PUBLIC'
    Usage: conn = sf_connection.sf_connection()
    '''
    p_key = serialization.load_pem_private_key(
        sfRSAKey.encode(), password=sfPassword, backend=default_backend())
    pkb = p_key.private_bytes(encoding=serialization.Encoding.DER,
                              format=serialization.PrivateFormat.PKCS8, encryption_algorithm=serialization.NoEncryption())
    connection = snowflake.connector.connect(
        account=account,
        role=role,
        user=sfUser,
        private_key=pkb,
        database=database,
        warehouse=warehouse,
        schema=schema
    )
    return connection

if __name__ == '__main__':
    connection = sf_connection()