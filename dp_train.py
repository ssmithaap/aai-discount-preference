"""
title:          dp_train.py
description:    Python module for creating discount sensitivity scores for identifiable customers
authors:        Scott Smith
date:           7/8/2021
verison:        1.0
python_version: 3.7.9
issues:
# ======================================================================
"""

import sys
sys.path.append('/home/ec2-user/corp.advanceai.cdf.config/artifacts/bdf-shared-code/') 
import ast
import numpy as np
import yaml
import pandas as pd
import s3fs
import xgboost as xgb
from functools import partial
from xgboost import XGBClassifier
from datetime import date, timedelta, datetime
from typing import List, Set, Dict, Tuple, Optional, Iterable, Callable, Union
from sf import sf_connection
from snowflake.connector.connection import SnowflakeConnection
from hyperopt import STATUS_OK, Trials, fmin, hp, tpe
import logging
import pyarrow
from snowflake.sqlalchemy import URL
from sqlalchemy import create_engine
import boto3
import logging
import warnings
import pyarrow.parquet as pq
from sklearn.metrics import roc_auc_score, average_precision_score
from sklearn.model_selection import train_test_split
import pickle
from alerting import alert
from helpers import *
from query_strings import *

def effify(non_f_str: str)-> str:
    """Function to instantiate values into an f-string"""
    return eval(f'f"""{non_f_str}"""')

# Set up the s3 file system for parquet reads, and ignore warning messages
s3 = s3fs.S3FileSystem()
warnings.filterwarnings('ignore')

# Logging to move down to error only
loggers_to_shut_up = [
    "snowflake.connector",
    "hyperopt.tpe",
    "hyperopt"
]
for logger in loggers_to_shut_up:
    logging.getLogger(logger).setLevel(logging.ERROR)

# Setup logging
FORMAT = '%(asctime)s - [%(name)s] - %(levelname)s %(message)s'
logging.basicConfig(format=FORMAT, level=logging.INFO)
logger = logging.getLogger(__name__)
logger.info("Reading the yaml file")

# Argument dictionary from json file for debug only
custom_vars = '{"EMAIL_ID": "s.smith@advance-auto.com", "YAML_FILE_NAME": "config.yaml", "DIAG_LABEL": "3quarter", "TEST": "True", "YES_ECOMM": "True", "NO_ECOMM": "True"}'

# Read in the json file
#custom_vars = sys.argv[1]
custom_vars = ast.literal_eval(custom_vars)
EMAIL_ID = custom_vars['EMAIL_ID']
YAML_FILE_NAME = custom_vars['YAML_FILE_NAME']
DIAG_LABEL = custom_vars['DIAG_LABEL']
TEST = eval(custom_vars['TEST'])
YES_ECOMM = eval(custom_vars['YES_ECOMM'])
NO_ECOMM = eval(custom_vars['NO_ECOMM'])
DIAG = True # this turns on labeling in all functions
logger.info("Finished reading in config from json file")
    
# Read in the yaml file
with open(YAML_FILE_NAME) as f:
    config = yaml.load(f, Loader=yaml.SafeLoader)
        
# Extract the input locations
if TEST:
    LABEL_PATH = config['DATA_DESTINATION']['LABEL_PATH_TEST']
    FEATURE_PATH = config['DATA_SOURCE']['FEATURE_PATH_TEST']
else:
    LABEL_PATH = config['DATA_DESTINATION']['LABEL_PATH']
    FEATURE_PATH = config['DATA_SOURCE']['FEATURE_PATH']
    
DIM_STORE = config['DATA_SOURCE']['DIM_STORE']
AAP_STORE_DIST = config['DATA_SOURCE']['AAP_STORE_DIST']
COMP_STORE_DIST = config['DATA_SOURCE']['COMP_STORE_DIST']
TENURE = config['DATA_SOURCE']['TENURE']
ECOMM_XREF = config['DATA_SOURCE']['ECOMM_XREF']

# Extract the feature lists
NO_FEATURES = config['COMMON_FEATURES']
YES_FEATURES = config['ECOMM_FEATURES'] + NO_FEATURES

# Extract the output locations
if TEST:
    MODEL_TRAINING_TABLE = config['DATA_DESTINATION']['MODEL_TRAINING_TABLE_TEST']
else:
    MODEL_TRAINING_TABLE = config['DATA_DESTINATION']['MODEL_TRAINING_TABLE']

# Pull the model information
time_stamp = int(datetime.now().strftime("%Y%m%d%H%M%S"))
logger.info(f'The timestamp for this run is {time_stamp}')
yes_name = f'yes_model_{time_stamp}_.mod'
no_name = f'no_model_{time_stamp}_.mod'
BUCKET = config['MODEL_TRAIN']['BUCKET']
YES_ECOMM_KEY = effify(config['MODEL_TRAIN']['YES_ECOMM_KEY'])
NO_ECOMM_KEY = effify(config['MODEL_TRAIN']['NO_ECOMM_KEY'])
MAX_EVALS = config['MODEL_TRAIN']['MAX_EVALS']
N_ESTIMATORS = config['MODEL_TRAIN']['N_ESTIMATORS']
N_JOBS = config['MODEL_TRAIN']['N_JOBS'] 
EARLY_STOPPING_ROUNDS = config['MODEL_TRAIN']['EARLY_STOPPING_ROUNDS']
logger.info("Finished reading in config from yaml file")

if __name__ == '__main__':
    try: 
        # Read the label table
        label_table = read_labels(LABEL_PATH)

        # Read the feature table from spark and the models (with associated information)
        feature_table, score_date = read_features(FEATURE_PATH)

        # Read other tables to add featurs
        aap_dist, comp_dist, tenure, ecomm_xref = read_extra_features(
            AAP_QUERY, COMP_QUERY, TENURE_QUERY, ECOMM_XREF_QUERY, effify, sf_connection())

        # Combine the features into a single table
        combined_table = combine_features(feature_table, aap_dist, comp_dist, tenure, label_table, DIAG)

        # Split into eComm engaged and non-eComm engaged
        yes_table, no_table, frac_positive_class_yes, frac_positive_class_no = split_data(combined_table)

        if YES_ECOMM:
            logger.info('Starting processing of eCommerce engaged model')
            # Tune eCommerce engaged customers
            best_hyperparms, X_train, X_test, y_train, y_test = tune_model(table=yes_table,
                                                                           features=YES_FEATURES,
                                                                           diag_label=DIAG_LABEL,
                                                                           max_evals=MAX_EVALS,
                                                                           n_estimators=N_ESTIMATORS,
                                                                           n_jobs=N_JOBS,
                                                                           early_stopping_rounds=EARLY_STOPPING_ROUNDS,
                                                                           space=SPACE,
                                                                           test_size=0.1)

            # Evaluste the eCommerce enaged model and fit on all the data
            fitted_model, aupr, auroc = evaluate_model_fit(hyper_parameters=best_hyperparms, 
                                   X_train=X_train, 
                                   X_test=X_test,
                                   y_train=y_train, 
                                   y_test=y_test,
                                   n_jobs=N_JOBS,
                                   n_estimators=N_ESTIMATORS)

            # Create diagnostics record
            diagnostics = pd.DataFrame({'TRAIN_DATE': score_date,
                                           'BUCKET': BUCKET,
                                           'MODEL_NAME': YES_ECOMM_KEY,
                                           'MAX_EVALS': MAX_EVALS,
                                           'N_ESTIMATORS': N_ESTIMATORS,
                                           'N_JOBS': N_JOBS, 
                                           'EARLY_STOPPING_ROUNDS': EARLY_STOPPING_ROUNDS,
                                           'POS_FRAC': frac_positive_class_yes,
                                           'AUPR': aupr,
                                           'AUROC': auroc},
                                           index=[0])

            # Save the model
            write_pickle_to_s3(BUCKET, YES_ECOMM_KEY, fitted_model)
            logger.info(f'Saved model {BUCKET}/{YES_ECOMM_KEY}')

            # Save the diagnostics record
            engine = create_engine(URL(account = 'aap.us-east-1.privatelink'), creator=lambda: sf_connection())
            with engine.connect() as write_connection:
                diagnostics.to_sql(MODEL_TRAINING_TABLE.lower(), con=write_connection, if_exists='append', index=False)
                logger.info('Finished writing eComm enaged model training log to snowflake  - training complete.')

        if NO_ECOMM:
            logger.info('Starting processing of non-eCommerce engaged model')
            # Tune eCommerce engaged customers
            best_hyperparms, X_train, X_test, y_train, y_test = tune_model(table=no_table,
                                                                           features=NO_FEATURES,
                                                                           diag_label=DIAG_LABEL,
                                                                           max_evals=MAX_EVALS,
                                                                           n_estimators=N_ESTIMATORS,
                                                                           n_jobs=N_JOBS,
                                                                           early_stopping_rounds=EARLY_STOPPING_ROUNDS,
                                                                           space=SPACE,
                                                                           test_size=0.1)

            # Evaluste the eCommerce enaged model and fit on all the data
            fitted_model, aupr, auroc = evaluate_model_fit(hyper_parameters=best_hyperparms, 
                                   X_train=X_train, 
                                   X_test=X_test,
                                   y_train=y_train, 
                                   y_test=y_test,
                                   n_jobs=N_JOBS,
                                   n_estimators=N_ESTIMATORS)

            # Create diagnostics record
            diagnostics = pd.DataFrame({'TRAIN_DATE': score_date,
                                           'BUCKET': BUCKET,
                                           'MODEL_NAME': YES_ECOMM_KEY,
                                           'MAX_EVALS': MAX_EVALS,
                                           'N_ESTIMATORS': N_ESTIMATORS,
                                           'N_JOBS': N_JOBS, 
                                           'EARLY_STOPPING_ROUNDS': EARLY_STOPPING_ROUNDS,
                                           'POS_FRAC': frac_positive_class_no,
                                           'AUPR': aupr,
                                           'AUROC': auroc},
                                           index=[0])

            # Save the model
            write_pickle_to_s3(BUCKET, NO_ECOMM_KEY, fitted_model)
            logger.info(f'Saved model {BUCKET}/{NO_ECOMM_KEY}')

            # Save the diagnostics record
            engine = create_engine(URL(account = 'aap.us-east-1.privatelink'), creator=lambda: sf_connection())
            with engine.connect() as write_connection:
                diagnostics.to_sql(MODEL_TRAINING_TABLE.lower(), con=write_connection, if_exists='append', index=False)
            logger.info('Finished writing non-eComm enaged model training log to snowflake  - training complete.')
        
        # Send success email
        alert(recipients = EMAIL_ID,
              subject = f'Model creation succeeded for discount personalization',
              message_body = f'Success creating models for date {time_stamp}')

    except Exception as e:
        # Send failure email
        alert(recipients = EMAIL_ID,
              subject = f'Failed to train models for discount personalization',
              message_body = f'Failed to  create models for date {time_stamp} due to error {e}')
        logger.exception(e, exc_info=True)
        raise